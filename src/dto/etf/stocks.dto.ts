import { IsEnum, IsNotEmpty, IsNumber, IsString, Min } from 'class-validator';
import { StocksSectionEnum } from '../../utils';

export class StocksDTOBase {
  @IsNotEmpty()
  @IsEnum(StocksSectionEnum)
  section: StocksSectionEnum;
  @IsNumber()
  @Min(0)
  @IsNotEmpty()
  stocks: number;
}

export class StocksDTOWithSymbol extends StocksDTOBase {
  @IsString()
  @IsNotEmpty()
  symbol: string;
}
