import { IsEnum, IsNotEmpty, IsNumber, Min } from 'class-validator';
import { BondsEnum, BondsSectionEnum } from '../utils';

export class BondsDTOBase {
  @IsNotEmpty()
  @IsEnum(BondsSectionEnum)
  @IsNumber()
  section: BondsSectionEnum;
  @IsNotEmpty()
  @Min(0)
  money: number;
}

export class BondsDTOWithType extends BondsDTOBase {
  @IsNotEmpty()
  @IsEnum(BondsEnum)
  @IsNumber()
  bondType: BondsEnum;
}
