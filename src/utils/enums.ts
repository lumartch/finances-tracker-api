export enum StocksSectionEnum {
  USA_STOCKS,
  DEVELOPED_ECON_STOCKS,
  DEVELOPING_ECON_STOCKS,
  REAL_STATE,
}

export enum BondsSectionEnum {
  BONDS,
  INFLATIONARY_BONDS,
}

export enum BondsEnum {
  CETES,
  BONOS,
  UDIBONO,
  BONDEF,
  BPAG28,
  BPAG91,
  BPAG182,
}

export enum BondsNameEnum {
  CETES = 'CETES',
  BONOS = 'BONOS',
  UDIBONO = 'UDIBONO',
  BONDEF = 'BONDEF',
  BPAG28 = 'BPAG28',
  BPAG91 = 'BPAG91',
  BPAG182 = 'BPAG182',
}

export enum ExchangeCurrency {
  USD = 'USD',
  MXN = 'MXN',
  EUR = 'EUR',
}
