import {
  BondsEnum,
  BondsNameEnum,
  BondsSectionEnum,
  StocksSectionEnum,
} from './enums';

export const PERCENTAGE = 100;

export const BONDS_STRING_TO_BONDS_ENUM = {
  [BondsNameEnum.BONDEF]: BondsEnum.BONDEF,
  [BondsNameEnum.BONOS]: BondsEnum.BONOS,
  [BondsNameEnum.BPAG182]: BondsEnum.BPAG182,
  [BondsNameEnum.BPAG28]: BondsEnum.BPAG28,
  [BondsNameEnum.BPAG91]: BondsEnum.BPAG91,
  [BondsNameEnum.CETES]: BondsEnum.CETES,
  [BondsNameEnum.UDIBONO]: BondsEnum.UDIBONO,
};

export const BONDSENUM_TO_BONDSSTRING = {
  [BondsEnum.BONDEF]: BondsNameEnum.BONDEF,
  [BondsEnum.BONOS]: BondsNameEnum.BONOS,
  [BondsEnum.BPAG182]: BondsNameEnum.BPAG182,
  [BondsEnum.BPAG28]: BondsNameEnum.BPAG28,
  [BondsEnum.BPAG91]: BondsNameEnum.BPAG91,
  [BondsEnum.CETES]: BondsNameEnum.CETES,
  [BondsEnum.UDIBONO]: BondsNameEnum.UDIBONO,
};

export const StocksSectionOrder: StocksSectionEnum[] = [
  StocksSectionEnum.USA_STOCKS,
  StocksSectionEnum.DEVELOPED_ECON_STOCKS,
  StocksSectionEnum.DEVELOPING_ECON_STOCKS,
  StocksSectionEnum.REAL_STATE,
];

export const StocksSectionEnumToObjective = {
  [StocksSectionEnum.USA_STOCKS]: 30,
  [StocksSectionEnum.DEVELOPED_ECON_STOCKS]: 15,
  [StocksSectionEnum.DEVELOPING_ECON_STOCKS]: 5,
  [StocksSectionEnum.REAL_STATE]: 20,
};

export const BondsSectionOrder: BondsSectionEnum[] = [
  BondsSectionEnum.BONDS,
  BondsSectionEnum.INFLATIONARY_BONDS,
];

export const BondsSectionEnumToObjective = {
  [BondsSectionEnum.BONDS]: 15,
  [BondsSectionEnum.INFLATIONARY_BONDS]: 15,
};
