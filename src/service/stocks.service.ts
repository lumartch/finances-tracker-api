import {
  BadRequestException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import { MongoRepository } from 'typeorm';
import yahooFinance from 'yahoo-finance2';
import { ResponseHandler } from '../config/';
import { StocksDTOBase, StocksDTOWithSymbol } from '../dto';
import { StocksEntity } from '../entities';
import { PERCENTAGE } from '../utils';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(StocksEntity)
    private readonly stocksRepository: MongoRepository<StocksEntity>,
  ) {}

  async postEtfInfo(stocksDTO: StocksDTOWithSymbol, response: Response) {
    stocksDTO.symbol = stocksDTO.symbol.toLowerCase();
    const _stockEntry = await this.findOneStock(stocksDTO.symbol);
    if (_stockEntry) {
      throw new BadRequestException({
        error: `Duplicated ETF symbol: '${stocksDTO.symbol}' is already assign to your account.`,
      });
    }
    try {
      await yahooFinance.quoteSummary(stocksDTO.symbol);
    } catch (_) {
      throw new NotFoundException({
        error: `ETF symbol '${stocksDTO.symbol}' not found.`,
      });
    }
    try {
      const stockEntry = await this.stocksRepository.save(stocksDTO);
      return ResponseHandler({
        response,
        value: stockEntry,
        status: HttpStatus.CREATED,
      });
    } catch (_) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }

  async getEtfInfo(symbol: string) {
    symbol = symbol.toLowerCase();
    try {
      const stockEntry = await this.stocksRepository.findOne({
        where: { symbol: symbol },
      });
      const { summaryDetail, price } = await yahooFinance.quoteSummary(symbol);
      return {
        name: price?.shortName,
        currency: summaryDetail?.currency,
        price: {
          current: price?.regularMarketPrice,
          dayHigh: price?.regularMarketDayHigh,
          dayLow: price?.regularMarketDayLow,
          open: price?.regularMarketOpen,
          change: price?.regularMarketChange,
          changePercent: price?.regularMarketChangePercent
            ? price?.regularMarketChange
            : 0 * PERCENTAGE,
        },
        symbol: price?.symbol,
        stocks: stockEntry !== null ? stockEntry?.stocks : 0,
      };
    } catch (error) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }

  async putEtfInfo(
    symbol: string,
    stocksDTO: StocksDTOBase,
    response: Response,
  ) {
    const _stockEntry = await this.findOneStock(symbol.toLowerCase());
    if (_stockEntry === null) {
      throw new NotFoundException({
        error: `ETF symbol not found: '${symbol}'.`,
      });
    }
    try {
      const _stock = this.stocksRepository.create(stocksDTO);
      await this.stocksRepository.update(
        {
          symbol: symbol,
        },
        { ..._stock },
      );
      return ResponseHandler({
        response,
        value: { ..._stock, symbol },
        status: HttpStatus.ACCEPTED,
      });
    } catch (_) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }

  async deleteEtfInfo(symbol: string, response: Response) {
    symbol = symbol.toLowerCase();
    try {
      await this.stocksRepository.delete({ symbol: symbol });
      return ResponseHandler({
        response,
        status: HttpStatus.OK,
      });
    } catch (error) {
      throw new InternalServerErrorException({
        error: 'Internal server error',
      });
    }
  }

  private async findOneStock(symbol: string) {
    try {
      return await this.stocksRepository.findOne({
        where: { symbol: symbol },
      });
    } catch (_) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }
}
