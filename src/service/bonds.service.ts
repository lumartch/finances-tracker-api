import {
  BadRequestException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import { ObjectId } from 'mongodb';
import { MongoRepository } from 'typeorm';
import { ResponseHandler } from '../config';
import { BondsDTOBase, BondsDTOWithType } from '../dto';
import { BondsEntity } from '../entities';
import {
  BONDSENUM_TO_BONDSSTRING,
  BONDS_STRING_TO_BONDS_ENUM,
  BondsEnum,
  BondsNameEnum,
} from '../utils';

@Injectable()
export class BondsService {
  constructor(
    @InjectRepository(BondsEntity)
    private readonly bondsRepository: MongoRepository<BondsEntity>,
  ) {}

  async getBondsInfo(bondType: string) {
    bondType = bondType.toUpperCase();
    const _bondType = BONDS_STRING_TO_BONDS_ENUM[bondType as BondsNameEnum];
    const _bond = await this.findByBondType(_bondType);
    if (_bond === null) {
      throw new NotFoundException({
        error: `Bond name not found: '${bondType}'.`,
      });
    }
    return _bond;
  }

  async postBondsInfo(bondsDTO: BondsDTOWithType, response: Response) {
    const _bond = await this.findByBondType(bondsDTO.bondType);
    if (_bond) {
      throw new BadRequestException({
        error: `Duplicated Bond type: '${
          BONDSENUM_TO_BONDSSTRING[_bond.bondType]
        }' is already assign to your account.`,
      });
    }
    try {
      const bondEntry = await this.bondsRepository.save(bondsDTO);
      return ResponseHandler({
        response,
        value: bondEntry,
        status: HttpStatus.CREATED,
      });
    } catch (_) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }

  async putBondsInfo(_id: string, bondsDTO: BondsDTOBase, response: Response) {
    const _bond = await this.bondsRepository.findOne({
      where: {
        _id: new ObjectId(_id),
      },
    });
    if (!_bond) {
      throw new NotFoundException({
        error: `Bond _id not found: '${_id}'.`,
      });
    }
    try {
      const bondEntry = this.bondsRepository.create(bondsDTO);
      await this.bondsRepository.update(
        { _id: new ObjectId(_id) },
        { money: bondEntry.money, section: bondEntry.section },
      );
      return ResponseHandler({
        response,
        value: { ...bondEntry, _id },
        status: HttpStatus.ACCEPTED,
      });
    } catch (error) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }

  async deleteBondInfo(_id: string, response: Response) {
    try {
      await this.bondsRepository.delete({ _id: new ObjectId(_id) });
      return ResponseHandler({
        response,
        status: HttpStatus.OK,
      });
    } catch (error) {
      throw new InternalServerErrorException({
        error: 'Internal server error',
      });
    }
  }

  private async findByBondType(bondType: BondsEnum) {
    try {
      return await this.bondsRepository.findOne({
        where: {
          bondType,
        },
      });
    } catch (e) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }
}
