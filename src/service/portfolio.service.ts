import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Convert } from 'easy-currencies';
import { MongoRepository } from 'typeorm';
import yahooFinance from 'yahoo-finance2';
import { BondsEntity, StocksEntity } from '../entities';
import { toPortfolio } from '../mappers';
import {
  BondsSectionEnum,
  BondsSectionEnumToObjective,
  ExchangeCurrency,
  StocksSectionEnum,
  StocksSectionEnumToObjective,
} from '../utils';

@Injectable()
export class PortfolioService {
  constructor(
    @InjectRepository(StocksEntity)
    private readonly stocksRepository: MongoRepository<StocksEntity>,

    @InjectRepository(BondsEntity)
    private readonly bondsRepository: MongoRepository<BondsEntity>,
  ) {}

  async getPortfolio() {
    try {
      const convert = await Convert().from(ExchangeCurrency.USD).fetch();

      const _bonds = await this.bondsRepository.find();
      const _stocks = await this.stocksRepository.find();
      const _simple_bonds = await this.getBondsBySection(
        _bonds,
        BondsSectionEnum.BONDS,
      );

      const _inflationary_bonds = await this.getBondsBySection(
        _bonds,
        BondsSectionEnum.INFLATIONARY_BONDS,
      );

      const _usa_Stocks = await convert
        .amount(
          await this.getStockPriceBySection(
            _stocks,
            StocksSectionEnum.USA_STOCKS,
          ),
        )
        .to(ExchangeCurrency.MXN);
      const _developed_econ = await convert
        .amount(
          await this.getStockPriceBySection(
            _stocks,
            StocksSectionEnum.DEVELOPED_ECON_STOCKS,
          ),
        )
        .to(ExchangeCurrency.MXN);

      const _developing_econ = await convert
        .amount(
          await this.getStockPriceBySection(
            _stocks,
            StocksSectionEnum.DEVELOPING_ECON_STOCKS,
          ),
        )
        .to(ExchangeCurrency.MXN);

      const _real_state = await convert
        .amount(
          await this.getStockPriceBySection(
            _stocks,
            StocksSectionEnum.REAL_STATE,
          ),
        )
        .to(ExchangeCurrency.MXN);

      const _total =
        _simple_bonds +
        _inflationary_bonds +
        _usa_Stocks +
        _developed_econ +
        _developing_econ +
        _real_state;

      return {
        [BondsSectionEnum[BondsSectionEnum.BONDS]]: {
          ...toPortfolio(
            _simple_bonds,
            _total,
            BondsSectionEnumToObjective[BondsSectionEnum.BONDS],
            BondsSectionEnum.BONDS,
          ),
        },
        [BondsSectionEnum[BondsSectionEnum.INFLATIONARY_BONDS]]: {
          ...toPortfolio(
            _inflationary_bonds,
            _total,
            BondsSectionEnumToObjective[BondsSectionEnum.INFLATIONARY_BONDS],
            BondsSectionEnum.INFLATIONARY_BONDS,
          ),
        },
        [StocksSectionEnum[StocksSectionEnum.USA_STOCKS]]: {
          ...toPortfolio(
            _usa_Stocks,
            _total,
            StocksSectionEnumToObjective[StocksSectionEnum.USA_STOCKS],
            StocksSectionEnum.USA_STOCKS,
          ),
        },
        [StocksSectionEnum[StocksSectionEnum.DEVELOPED_ECON_STOCKS]]: {
          ...toPortfolio(
            _developed_econ,
            _total,
            StocksSectionEnumToObjective[
              StocksSectionEnum.DEVELOPED_ECON_STOCKS
            ],
            StocksSectionEnum.DEVELOPED_ECON_STOCKS,
          ),
        },
        [StocksSectionEnum[StocksSectionEnum.DEVELOPING_ECON_STOCKS]]: {
          ...toPortfolio(
            _developing_econ,
            _total,
            StocksSectionEnumToObjective[
              StocksSectionEnum.DEVELOPING_ECON_STOCKS
            ],
            StocksSectionEnum.DEVELOPING_ECON_STOCKS,
          ),
        },
        [StocksSectionEnum[StocksSectionEnum.REAL_STATE]]: {
          ...toPortfolio(
            _real_state,
            _total,
            StocksSectionEnumToObjective[StocksSectionEnum.REAL_STATE],
            StocksSectionEnum.REAL_STATE,
          ),
        },
        total: _total,
      };
    } catch (_) {
      throw new InternalServerErrorException({
        error: 'Internal server error.',
      });
    }
  }

  private async getBondsBySection(
    _bonds: BondsEntity[],
    _section: BondsSectionEnum,
  ) {
    return _bonds
      .filter(({ section }: BondsEntity) => section === _section)
      .flatMap(({ money }: BondsEntity) => money)
      .reduce((previous, current) => (previous += current), 0);
  }

  private async getStockPriceBySection(
    _stocks: StocksEntity[],
    _section: StocksSectionEnum,
  ) {
    const _stock_prices = await this._getStockPriceBySection(_stocks, _section);
    return _stock_prices.reduce(
      (previous, current) => (previous += current),
      0,
    );
  }

  private async _getStockPriceBySection(
    _stocks: StocksEntity[],
    _section: StocksSectionEnum,
  ) {
    return await Promise.all(
      _stocks
        .filter(({ section }: StocksEntity) => section === _section)
        .flatMap(async ({ stocks, symbol }: StocksEntity) => {
          const { price } = await yahooFinance.quoteSummary(symbol);
          return price?.regularMarketPrice !== undefined
            ? price.regularMarketPrice * stocks
            : 0;
        }),
    );
  }
}
