import { HttpStatus } from '@nestjs/common';
import { Response } from 'express';

interface IResponseHandler {
  response: Response<any, Record<string, any>>;
  status: HttpStatus;
  value?: any;
}

export const ResponseHandler = ({
  response,
  status,
  value,
}: IResponseHandler) =>
  response.status(status).send({
    ...(value && { ...value }),
  });
