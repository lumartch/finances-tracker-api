import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PortfolioController } from '../../controller';
import { BondsEntity, StocksEntity } from '../../entities';
import { PortfolioService } from '../../service';

@Module({
  imports: [TypeOrmModule.forFeature([StocksEntity, BondsEntity])],
  providers: [PortfolioService],
  controllers: [PortfolioController],
})
export class PortfolioModule {}
