import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BondsController } from '../../controller';
import { BondsEntity } from '../../entities';
import { BondsService } from '../../service';

@Module({
  imports: [TypeOrmModule.forFeature([BondsEntity])],
  providers: [BondsService],
  controllers: [BondsController],
})
export class BondsModule {}
