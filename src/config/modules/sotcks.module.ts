import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StocksController } from '../../controller';
import { StocksEntity } from '../../entities';
import { StocksService } from '../../service';

@Module({
  imports: [TypeOrmModule.forFeature([StocksEntity])],
  providers: [StocksService],
  controllers: [StocksController],
})
export class StocksModule {}
