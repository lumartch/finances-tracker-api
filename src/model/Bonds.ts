import { BondsSectionEnum } from '../utils';

export interface Bonds {
  name: string;
  section: BondsSectionEnum;
  money: number;
}
