import { StocksSectionEnum } from '../../utils';

export interface Stocks {
  symbol: string;
  section: StocksSectionEnum;
  stocks: number;
}
