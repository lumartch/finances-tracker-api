export interface Prices {
  current: number;
  open: number;
  dayLow: number;
  dayHigh: number;
  changePercent: number;
  change: number;
}
