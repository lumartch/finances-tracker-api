import { Prices } from './Prices';
import { Stocks } from './Stocks';

export interface Etf {
  currency: string;
  name: string;
  price: Prices;
  stocks?: Stocks;
  symbol: string;
}
