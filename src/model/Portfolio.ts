import { BondsSectionEnum, StocksSectionEnum } from '../utils';

export interface Portfolio {
  section: StocksSectionEnum | BondsSectionEnum;
  objective: number;
  actual: number;
  difference: number;
  nextInvestment: number;
  newObjective: number;
}
