import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { StocksDTOBase, StocksDTOWithSymbol } from '../dto';
import { StocksService } from '../service';

@Controller('/finances/1/portfolio/etf')
export class StocksController {
  constructor(private readonly stocksService: StocksService) {}

  @Post('/')
  postEtf(@Body() stocks: StocksDTOWithSymbol, @Res() response: Response) {
    return this.stocksService.postEtfInfo(stocks, response);
  }

  @Get('/:symbol')
  getEtf(@Param('symbol') symbol: string) {
    return this.stocksService.getEtfInfo(symbol);
  }

  @Put('/:symbol')
  putEtf(
    @Param('symbol') symbol: string,
    @Body() stocks: StocksDTOBase,
    @Res() response: Response,
  ) {
    return this.stocksService.putEtfInfo(symbol, stocks, response);
  }

  @Delete('/:symbol')
  deleteEtf(@Param('symbol') symbol: string, @Res() response: Response) {
    return this.stocksService.deleteEtfInfo(symbol, response);
  }
}
