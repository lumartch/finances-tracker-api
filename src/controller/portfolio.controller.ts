import { Controller, Get } from '@nestjs/common';
import { PortfolioService } from '../service';

@Controller('/finances/1/portfolio')
export class PortfolioController {
  constructor(private readonly portfolioService: PortfolioService) {}
  @Get('/')
  getPortfolio() {
    return this.portfolioService.getPortfolio();
  }
}
