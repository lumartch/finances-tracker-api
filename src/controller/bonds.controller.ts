import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { BondsDTOBase, BondsDTOWithType } from '../dto';
import { BondsService } from '../service';

@Controller('/finances/1/portfolio/bonds')
export class BondsController {
  constructor(private readonly bondService: BondsService) {}

  @Post('/')
  postBonds(@Body() bondsDTO: BondsDTOWithType, @Res() response: Response) {
    return this.bondService.postBondsInfo(bondsDTO, response);
  }

  @Get('/:name')
  getBonds(@Param('name') name: string) {
    return this.bondService.getBondsInfo(name);
  }

  @Put('/:id')
  putBonds(
    @Param('id') id: string,
    @Body() bondsDTO: BondsDTOBase,
    @Res() response: Response,
  ) {
    return this.bondService.putBondsInfo(id, bondsDTO, response);
  }

  @Delete('/:id')
  deleteBond(@Param('id') _id: string, @Res() response: Response) {
    return this.bondService.deleteBondInfo(_id, response);
  }
}
