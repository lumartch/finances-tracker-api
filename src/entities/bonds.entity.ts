import { Column, Entity, ObjectId, ObjectIdColumn } from 'typeorm';
import { BondsEnum, BondsSectionEnum } from '../utils';

@Entity({ schema: 'bonds', name: 'bonds' })
export class BondsEntity {
  @ObjectIdColumn()
  _id: ObjectId;
  @Column()
  bondType: BondsEnum;
  @Column()
  section: BondsSectionEnum;
  @Column()
  money: number;
}
