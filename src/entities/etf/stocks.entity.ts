import { Column, Entity, Index, ObjectIdColumn } from 'typeorm';
import { StocksSectionEnum } from '../../utils';

@Entity({ schema: 'stocks', name: 'stocks' })
export class StocksEntity {
  @ObjectIdColumn()
  _id: string;
  @Column()
  symbol: string;
  @Index()
  @Column()
  section: StocksSectionEnum;
  @Column()
  stocks: number;
}
