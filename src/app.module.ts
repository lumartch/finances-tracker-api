import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BondsModule, PortfolioModule, StocksModule } from './config';
import { BondsEntity, StocksEntity } from './entities';

@Module({
  imports: [
    BondsModule,
    StocksModule,
    PortfolioModule,
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'],
    }),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: process.env.DB_CONN_STRING,
      entities: [StocksEntity, BondsEntity],
      logging: true,
      synchronize: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
