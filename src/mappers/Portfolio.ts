import { Portfolio } from '../model';
import { BondsSectionEnum, StocksSectionEnum } from '../utils';

export function toPortfolio(
  actual: number,
  total: number,
  objective: number,
  section: StocksSectionEnum | BondsSectionEnum,
): Portfolio {
  return {
    actual,
    difference: 0,
    newObjective: 0,
    nextInvestment: 0,
    objective,
    section,
  };
}
